import 'package:flutter/material.dart';
import 'package:readStories/model/categories.dart';
import 'package:readStories/repository/categories_repository.dart';

class Categories extends StatefulWidget {
  Categories({Key key}) : super(key: key);

  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  CategoriesModel data;

  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() async {
    data = await getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
    );
  }
}
