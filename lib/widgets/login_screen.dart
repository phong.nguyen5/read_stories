import 'package:flutter/material.dart';
import 'package:readStories/model/login_form.dart';
import 'package:readStories/repository/authentication_repository.dart'
    as authenRepo;
import 'categories_screen.dart';
import 'utils/rounded_button.dart';
import 'utils/rounded_input_field.dart';
import 'utils/rounded_password_field.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginForm form;

  @override
  void initState() {
    super.initState();
    form = LoginForm();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: Container(
      width: double.infinity,
      height: size.height,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "LOGIN",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: size.height * 0.03),
                RoundedInputField(
                  hintText: "Your Email",
                  onChanged: (value) {
                    form.email = value;
                  },
                ),
                RoundedPasswordField(
                  onChanged: (value) {
                    form.password = value;
                  },
                ),
                RoundedButton(
                  text: "LOGIN",
                  press: login,
                ),
              ],
            ),
          ),
        ],
      ),
    ));
  }

  void login() async {
    // validate != null
    // if (true) {
    //   var result = await authenRepo.login(form);
    //   if (result.code == 200) {
    //     // push to screen home
    //   }
    // }
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Categories()),
    );
  }
}
