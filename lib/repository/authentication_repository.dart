import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:readStories/model/login.dart';
import 'package:readStories/model/login_form.dart';

Future<LoginModel> login(LoginForm request, {String language}) async {
  var dio = Dio();
  var response = await dio.post('url', data: request.toJson());
  var data;
  if(response.statusCode == 200 || response.statusCode == 201){
    data = LoginModel.fromJson(json.decode(response.data));
  }else{
    // login false
  }
  return data;
}
