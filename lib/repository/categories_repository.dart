import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:readStories/model/categories.dart';

Future<CategoriesModel> getCategories({String language}) async {
  var dio = Dio();
  var response = await dio.get('url');
  var data;
  if(response.statusCode == 200 || response.statusCode == 201){
    data = CategoriesModel.fromJson(json.decode(response.data));
  }else{
    // login false
  }
  return data;
}