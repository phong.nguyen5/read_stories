class CategoriesModel {
  int code;
  String message;
  List<Categorie> result;

  CategoriesModel({
    this.code,
    this.message,
    this.result,
  });

  CategoriesModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    result = json['result'];
  }
}

class Categorie {
  int id;
  String categoryName;

  Categorie({
    this.id,
    this.categoryName,
  });

  Categorie.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    categoryName = json['category_name'];
  }
}

