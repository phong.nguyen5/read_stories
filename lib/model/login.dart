class LoginModel {
  int code;
  String message;
  String accessToken;
  String tokenType;

  LoginModel({
    this.accessToken,
    this.tokenType,
    this.code,
    this.message,
  });

  LoginModel.fromJson(Map<String, dynamic> json) {
    accessToken = json['access_token'];
    tokenType = json['token_type'];
    code = json['code'];
    message = json['message'];
  }
}
