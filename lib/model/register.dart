class RegisterModel {
  int code;
  String message;
  RegisterResult result;

  RegisterModel({
    this.code,
    this.message,
    this.result,
  });

  RegisterModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    result = json['result'];
  }
}

class RegisterResult {
  List<String> email;
  String accessToken;
  String tokenType;

  RegisterResult({
    this.email,
    this.accessToken,
    this.tokenType,
  });

  RegisterResult.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    accessToken = json['access_token'];
    tokenType = json['token_type'];
  }
}

